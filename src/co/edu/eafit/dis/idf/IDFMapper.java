package co.edu.eafit.dis.idf;



import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class IDFMapper extends Mapper<LongWritable,Text,Text,IntWritable> {

    private Text word = new Text();
    private Text filename = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{


    		String line = value.toString();
            
            
            StringTokenizer tokenizer = new StringTokenizer(line,"\n");
            
            while (tokenizer.hasMoreTokens()) {
            
            	String outputKey = tokenizer.nextToken();
            	String [] kv = outputKey.split("\\s+|\\s+");
                String [] wordFile = kv[0].split("@");
                context.write(new Text(wordFile[0]), new IntWritable(1));
                
            }

    }

}
