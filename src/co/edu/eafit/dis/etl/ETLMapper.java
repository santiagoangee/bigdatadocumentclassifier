package co.edu.eafit.dis.etl;



import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class ETLMapper extends Mapper<LongWritable,Text,Text,Text> {

	private Text txtKey = new Text("");
	private Text txtValue = new Text("");
	
	private TreeMap<String,String> lemmas =
			ETL.parseLemmas("/home/sangeea/git/BigDataDocumentClassifier/data/sorted_lemmas.txt");

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		FileSplit currentSplit = ((FileSplit) context.getInputSplit());
        String filenameStr = currentSplit.getPath().getName();
        String line = value.toString();
        
        txtKey.set(filenameStr);
        
		String result = ETL.etlLine(line, lemmas);
		txtValue.set(result);
		context.write(txtKey, txtValue);
		
	}

}
