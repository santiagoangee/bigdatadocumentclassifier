package co.edu.eafit.dis.etl;

import java.io.IOException;

public class ETLMain {

	public static void main(String[] args) {
		
		ETL etl = new ETL(args[0], args[1]);
		try {
		  etl.etl(args[0]);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}

}
