package co.edu.eafit.dis.etl;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

public class ETLReducer extends Reducer<Text, Text,Text,Text> {
    
	private MultipleOutputs mos;

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		mos = new MultipleOutputs(context);

	}

	@Override
	public void reduce(final Text key, final Iterable<Text> values,
			final Context context)
			throws IOException, InterruptedException {

		for (Text value : values) {
			mos.write(key, value, key.toString());

		}
	}

	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {
		mos.close();
	}
	
}