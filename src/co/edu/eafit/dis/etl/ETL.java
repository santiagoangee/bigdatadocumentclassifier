package co.edu.eafit.dis.etl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.es.SpanishAnalyzer;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

public class ETL {
	

  /* This method removes the direct and indirect objects in a word
   * */

  private String lemmasPath;
  private String inputDir;
  private List<String> files;
  
  
  //Regular expression to remove punctuation marks
  private final static Pattern PUNCTUATION = Pattern.compile("[^a-zA-Z ]");
	
  private TreeMap<String,String> lemmas;
  public final static CharArraySet stopSet = SpanishAnalyzer.getDefaultStopSet();

  
  public ETL(String inputDir, String lemmasPath) {
	  
	  this.inputDir = inputDir;
	  this.lemmasPath = lemmasPath;
	  
	  lemmas = parseLemmas(this.lemmasPath);
  }
  
  public static TreeMap<String,String> parseLemmas(String lemmasPath) {
	  List<String> lemmasString = new ArrayList<String>();
	  TreeMap<String,String> lemmas = new TreeMap<String,String>();
	
	  File file = new File(lemmasPath);
  
	  if(file.exists()){
		  try { 
			  lemmasString= Files.readAllLines(file.toPath(),Charset.defaultCharset());
		  } catch (IOException ex) {
			  ex.printStackTrace();
		  }
		  if(lemmasString.isEmpty()) {
			System.err.println("Error: lemmas file is empty");
	      	return null;
		  }
	  }
	
	  for(String line : lemmasString){
		  String [] res = line.split("\\s+");
		  lemmas.put(res[0], res[1]);
	  }
	
	  return lemmas;

  }

  public static String removePronouns(String word, TreeMap<String,String> lemmas) {
	  
	  if (word.length() < 4) {
		  return word;
	  }
	  if (word.charAt(word.length() - 3) == 'r' &&
			  (word.substring(word.length() -2, word.length()).equals("se") ||
					  word.substring(word.length() -2, word.length()).equals("te") ||
					  word.substring(word.length() -2, word.length()).equals("me") ||
					  word.substring(word.length() -2, word.length()).equals("os") ||
					  word.substring(word.length() -2, word.length()).equals("lo") ||
					  word.substring(word.length() -2, word.length()).equals("la") ||
					  word.substring(word.length() -2, word.length()).equals("le"))) {
		  return word.substring(0, word.length() - 2);
	  }
	  
	  if (word.charAt(word.length() - 4) == 'r' &&
			  (word.substring(word.length() -3, word.length()).equals("los") ||
					  word.substring(word.length() -3, word.length()).equals("las") ||
					  word.substring(word.length() -3, word.length()).equals("les"))) {
		  return word.substring(0, word.length() - 3);
	  }
	  
	  if (word.charAt(word.length() - 3) != 'r' &&
			  (word.substring(word.length() -2, word.length()).equals("se") ||
					  word.substring(word.length() -2, word.length()).equals("te") ||
					  word.substring(word.length() -2, word.length()).equals("me") ||
					  word.substring(word.length() -2, word.length()).equals("os") ||
					  word.substring(word.length() -2, word.length()).equals("lo") ||
					  word.substring(word.length() -2, word.length()).equals("la") ||
					  word.substring(word.length() -2, word.length()).equals("le"))) {
		  String newWord = stemWord(word, lemmas);
		  
		  return newWord;
	  }
	  
	  if (word.charAt(word.length() - 4) != 'r' &&
			  (word.substring(word.length() -3, word.length()).equals("los") ||
					  word.substring(word.length() -3, word.length()).equals("las") ||
					  word.substring(word.length() -3, word.length()).equals("les"))) {
		  
		  String newWord = stemWord(word, lemmas);
		  return newWord;
	  }
	  
	  return word;
  
  }
	
  public static String stemWord(String word, TreeMap<String,String> lemmas) {
	  
	  if (lemmas.containsKey(word)) {
		  return lemmas.get(word);
	  }
	  
	  return word;
  }
  
  public static void writeSortedLemmas(HashMap<String,String> lemmas) throws IOException {
	// Sort hashmap:
		lemmas = lemmas.entrySet().stream()
		                    .sorted(Map.Entry.<String,String>comparingByKey())
		                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		
		PrintWriter out = new PrintWriter("/home/sangeea/sorted_lemmas.txt", "UTF-8");
		for (Map.Entry<String, String> entry: lemmas.entrySet()) {
			
			out.print(entry.getKey() + " " + entry.getValue() + "\n");
			
		}
		out.close();
		
  }
  
  public static List<String> getDirFiles(String inputDir) {
	  List<String> files = new ArrayList<String>();
		
		
		File folder = new File(inputDir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
		    if (listOfFiles[i].isFile()) {
		      files.add(listOfFiles[i].getPath());
		    }
		}
		
		return files;
  }
  
  public static void etlFile(String filePath, TreeMap<String,String> lemmas) throws IOException {
	  
	  String fileName = Paths.get(filePath).getFileName().toString();
	  String outputDirPath = Paths.get(filePath).getParent() + "/" + "output";
	  String fileNameWithoutExtension = fileName.substring(0,fileName.length()-4);
	  PrintWriter out = new PrintWriter(outputDirPath + "/" + fileNameWithoutExtension + "__etl.txt", "UTF-8");
      PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new FileReader(filePath),
              new CoreLabelTokenFactory(), "");
      
      int lineOrganizer = 1;
      while (ptbt.hasNext()) {
        CoreLabel label = ptbt.next();
        
        String word = label.word().toLowerCase();
        
        // Match against punctuation strings
        Matcher m = PUNCTUATION.matcher(word);
        
        //Remove punctuation Strings
        if (!m.find()) {
        	if (!stopSet.contains(word)) {
        		if (ptbt.hasNext()) {
        		  String output = stemWord(word, lemmas);
        		  output = removePronouns(output, lemmas);
        		  if (lineOrganizer % 21 != 0) {
        		      out.print(output + " ");
        		  } else {
        			out.println(output);
        		  }
        		} else {
        			out.print(stemWord(word, lemmas));
        		}
        		lineOrganizer++;
        	}
        	
        }
        
      }
      out.close();
    }
  
  public static String etlLine(String line, TreeMap<String,String> lemmas) {

	  String result = "";
      PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new StringReader(line),
              new CoreLabelTokenFactory(), "");
      
      while (ptbt.hasNext()) {
        CoreLabel label = ptbt.next();
        
        String word = label.word().toLowerCase();
        
        // Match against punctuation strings
        Matcher m = PUNCTUATION.matcher(word);
        
        //Remove punctuation Strings
        if (!m.find()) {
        	if (!stopSet.contains(word)) {
        		if (ptbt.hasNext()) {
        		  String output = stemWord(word, lemmas);
        		  output = removePronouns(output, lemmas);
        		  result = result + output + " ";
        		} else {
        		  result = result + stemWord(word,lemmas);
        		}
        	}
        	
        }
        
      }
      
      return result;
  }
  

  public void etl(String inputDir) throws IOException {
		
	List<String> files = new ArrayList<String>();
	
	
	File folder = new File(inputDir);
	File[] listOfFiles = folder.listFiles();

	for (int i = 0; i < listOfFiles.length; i++) {
	    if (listOfFiles[i].isFile()) {
	      files.add(listOfFiles[i].getPath());
	    }
	}
	
	String outputDirString = inputDir + "/output";
    File outputDir = new File(outputDirString);
    if(outputDir.exists()) {
  	  System.err.println("There was an error creating the directory. It may be that the directory already exists");
  	  return;
    } else {
  	  System.out.println("Creating output directory: " + outputDir.getName());
  	  boolean result = false;
  	  
  	  outputDir.mkdir();
  	  result = true;
  	  
  	  if (result) {
      	  System.out.println("Directory created");
        }
    }
	
    for (String file: files) {
		try {
			ETL.etlFile(file, lemmas);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
  }
}